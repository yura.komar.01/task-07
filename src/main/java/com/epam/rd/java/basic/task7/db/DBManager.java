package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private DBManager() {
	}
	private static final String DB = "jdbc:derby:memory:testdb;create=true";

	public static synchronized DBManager getInstance() {
		if (instance==null)
			instance = new DBManager();
		return instance;
	}

	private Connection getConnection(){
		loadDriver();
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(DB);
		}catch (SQLException e){
			e.printStackTrace();
		}
		return connection;
	}

	private void loadDriver(){
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch (ClassNotFoundException e){
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> listOfUsers = new ArrayList<>();
		String sql = "SELECT * FROM Users";
		try(ResultSet resultSet = getConnection().createStatement().executeQuery(sql)) {
			while(resultSet.next()){
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("Login"));
				listOfUsers.add(user);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return listOfUsers;
	}

	public boolean insertUser(User user) throws DBException {
		String sql = "INSERT INTO users VALUES (DEFAULT, ?)";
		try(PreparedStatement statement = getConnection().prepareStatement(sql)){
			statement.setString(1, user.getLogin());
			statement.executeUpdate();
			return true;
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		String sql = "DELETE FROM users WHERE login = ?";
		for (User u : users) {
			try(PreparedStatement statement = getConnection().prepareStatement(sql)) {
				statement.setString(1, u.getLogin());
				statement.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		try(PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM users WHERE login = ?")) {
			statement.setString(1,login);
			ResultSet result = statement.executeQuery();
			User user = new User();
			while (result.next()) {
				user.setId(result.getInt(1));
				user.setLogin(result.getString(2));
			}
			return user;
		}catch (Exception e){e.printStackTrace();}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		try(PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM teams WHERE name = ?")) {
			statement.setString(1,name);
			Team team = new Team();
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				team.setId(result.getInt(1));
				team.setName(result.getString(2));
			}
			return team;
		}catch (Exception e){e.printStackTrace();}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		String sql = "SELECT * FROM teams";
		try(ResultSet resultSet = getConnection().createStatement().executeQuery(sql)) {
			while(resultSet.next()){
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));

				teams.add(team);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (team == null) return false;
		String sql = "INSERT INTO teams(name) VALUES(?)";
		try (
				Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		) {
			preparedStatement.setString(1, team.getName());
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) return false;

			try (ResultSet generatedKeysResult = preparedStatement.getGeneratedKeys()) {
				if (generatedKeysResult.next()) team.setId(generatedKeysResult.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try(Connection con = getConnection()) {

			String sql = "INSERT INTO users_teams values";
			for (int i = 0; i < teams.length; i++) {
				if(i > 0)sql = sql + " , ";
				sql =  sql + "(" + getUser(user.getLogin()).getId() + " , " + getTeam(teams[i].getName()).getId() + ")";
			}
			con.setAutoCommit(false);

			try(PreparedStatement posted = con.prepareStatement(sql)) {
				posted.executeUpdate();
			}catch (Exception ex){
				con.commit();
				con.setAutoCommit(true);
				throw ex;
			}
			con.commit();
			con.setAutoCommit(true);
		}catch (Exception e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try(PreparedStatement statement = getConnection().prepareStatement("SELECT teams.name from users_teams inner join teams on teams.id = team_id inner join users on users.id = user_id where users.login = ?")) {
			statement.setString(1,user.getLogin());
			ResultSet result = statement.executeQuery();
			List<Team> teams = new ArrayList<>();
			while (result.next()) {
				teams.add(Team.createTeam(result.getString(1)));
			}
			return teams;
		}catch (Exception e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}
	//test 6
	public boolean deleteTeam(Team team) throws DBException {
		String sql = "DELETE FROM teams WHERE name = ?";
		try(PreparedStatement statement = getConnection().prepareStatement(sql)) {
			statement.setString(1, team.getName());
			statement.executeUpdate();
			return true;
		}catch (Exception e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try(PreparedStatement posted = getConnection().prepareStatement("UPDATE teams SET name = ? WHERE id = ?")) {
			posted.setString(1,team.getName());
			posted.setInt(2,team.getId());
			posted.executeUpdate();
		}catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return true;
	}
}
